/* 
 * File:   main.c
 * Author: Matt Christensen
 *
 * Instructions:
In the programming language of your choice, write a program 
generating the first n Fibonacci numbers F(n), printing ...
- ... "Buzz" when F(n) is divisible by 3.
- ... "Fizz" when F(n) is divisible by 5.
- ... "BuzzFizz" when F(n) is prime.
- ... the value F(n) otherwise.
 * 
 * Notes:
 * I am treating this instruction as an order of priority.  Ie: if a number is divisible by 3 and 5, it will print "Buzz" not "Fizz"
 * I am using signed 64 bit integers, values about (2^63 - 1) will overflow and return an error.  
 * To support larger integers I would store the numbers in decimal string format instead of binary.
 */

//******************************************************************************
// Includes
//******************************************************************************
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <math.h>
#include <string.h>
#include <stdint.h>
#include <inttypes.h>

//******************************************************************************
// Definitions
//******************************************************************************
#define MAXIMUM_OUTPUT_STR_LEN  50

typedef enum {
    FALSE = 0,
    TRUE = 1,
} boolean;

typedef enum {
    BUZZ,
    FIZZ,
    BUZZFIZZ,
    FIZZ_BUZZ_N_VALUE,
} fizz_buzz_output;

//******************************************************************************
// Prototypes
//******************************************************************************
void runUnitTests(void);

//******************************************************************************
// Private Functions
//******************************************************************************
int64_t parseCommandLineInput(int argc, const char** argv) {
    if (argc != 2) return -1;
    char *endptr;
    int input = strtoll(argv[1], &endptr, 0);
    boolean nonNumberInput = *endptr ? TRUE : FALSE;  // endptr should point at the null terminator for a valid numeric input
    if ((input < 0) || nonNumberInput) return -1;    
    return input;
}

boolean isPrime(int64_t n) {
    if (n <= 1) return FALSE;
    int64_t squareRootOfN = sqrt(n);
    int64_t i;
    for (i=2; i<=squareRootOfN; i++) {
        if ((n % i) == 0) return FALSE;
    }
    return TRUE;
}

fizz_buzz_output fizzBuzz(int64_t n) {
    assert(n >= 0);    
    if ((n % 3) == 0) return BUZZ;
    if ((n % 5) == 0) return FIZZ;
    if (isPrime(n)) return BUZZFIZZ;
    return FIZZ_BUZZ_N_VALUE;
}

void fizzBuzzStr(char* str, int64_t n) {
    switch (fizzBuzz(n)) {
        case BUZZ:
            strcpy(str, "Buzz");
            break;
        case FIZZ:
            strcpy(str, "Fizz");
            break;
        case BUZZFIZZ:
            strcpy(str, "BuzzFizz");
            break;
        case FIZZ_BUZZ_N_VALUE:
            sprintf(str, "%" PRId64, n);
            break;
        default:
            assert(FALSE);
            break;        
    }    
}

int64_t nextFibonacciNumber(int64_t n, int64_t prevFibo0, int64_t prevFibo1) {
    if (n <= 0) return 0;
    if (n == 1) return 1;
    return prevFibo0 + prevFibo1;
}

void printFizzBuzzOutput(int64_t n) {
    char outputStr[MAXIMUM_OUTPUT_STR_LEN];
    fizzBuzzStr(outputStr, n);
    printf("%s\n", outputStr);
}

void printUsage(void) {
    printf("Usage: fibofizzbuzz non_negative_n\n");
}

// Main loop that solves fibonacci numbers and prints appropriate output
// return 0 if success, 1 if failure
int8_t performFiboFizzBuzz(int64_t n) {
    int64_t i;
    int64_t prev0 = 0;
    int64_t prev1 = 0;
    for (i=0; i<n; i++) {
        int64_t next = nextFibonacciNumber(i, prev0, prev1);
        if (next < 0) { // overflow
            printf("Overflow Error.\n");
            return EXIT_FAILURE;
        }
        prev0 = prev1;
        prev1 = next;
        printFizzBuzzOutput(next);
    }    
    return EXIT_SUCCESS;
}

int main(int argc, const char** argv) {    
    runUnitTests();
    int64_t input = parseCommandLineInput(argc, argv);
    if (input < 0) {
        printUsage();
        return EXIT_FAILURE;
    }
    return performFiboFizzBuzz(input);
}

//******************************************************************************
// Unit Tests
//******************************************************************************
void assertStrEqual(const char* a, const char* b) {
    assert(strcmp(a, b) == 0);
}

void testInput(int argc, const char** argv, int answer) {
    assert(parseCommandLineInput(argc, argv) == answer);
}

void testInputParser(void) {
    // If argc != 2, return error.
    const char* testInputs[] = {"fibofizzbuzz", "1"};
    testInput(-2, testInputs, -1);
    testInput(0, testInputs, -1);
    testInput(1, testInputs, -1);
    testInput(3, testInputs, -1);
    
    // If argument is negative, return error.
    const char* inputMinus[] = {"fibofizzbuzz", "-6"};
    testInput(2, inputMinus, -1);
    
    // If argument is non number, return error.
    const char* inputNonNumber[] = {"fibofizzbuzz", "asdf6"};
    testInput(2, inputNonNumber, -1);
    
    // If arguments are correct, return correct answer
    const char* input0[] = {"fibofizzbuzz", "0"};
    const char* input1[] = {"fibofizzbuzz", "1"};
    const char* input2[] = {"fibofizzbuzz", "32767"};
    testInput(2, input0, 0);
    testInput(2, input1, 1);
    testInput(2, input2, 32767);
}

void testIsPrime() {
    int64_t first168Primes[] = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 179, 181, 191, 193, 197, 199, 211, 223, 227, 229, 233, 239, 241, 251, 257, 263, 269, 271, 277, 281, 283, 293, 307, 311, 313, 317, 331, 337, 347, 349, 353, 359, 367, 373, 379, 383, 389, 397, 401, 409, 419, 421, 431, 433, 439, 443, 449, 457, 461, 463, 467, 479, 487, 491, 499, 503, 509, 521, 523, 541, 547, 557, 563, 569, 571, 577, 587, 593, 599, 601, 607, 613, 617, 619, 631, 641, 643, 647, 653, 659, 661, 673, 677, 683, 691, 701, 709, 719, 727, 733, 739, 743, 751, 757, 761, 769, 773, 787, 797, 809, 811, 821, 823, 827, 829, 839, 853, 857, 859, 863, 877, 881, 883, 887, 907, 911, 919, 929, 937, 941, 947, 953, 967, 971, 977, 983, 991, 997};
    int64_t i;
    for (i=0; i<168; i++) {
        assert(isPrime(first168Primes[i]) == TRUE);
        int64_t previous, j;
        if (i == 0) previous = 0;
        else previous = first168Primes[i-1];
        for (j=previous+1; j<first168Primes[i]; j++) {
            assert(isPrime(j) == FALSE);
        }
    }
}

void testFizzBuzz() {
    char testStr[MAXIMUM_OUTPUT_STR_LEN];
    
    // buzz when divisible by 3    
    assert(fizzBuzz(3) == BUZZ);
    assert(fizzBuzz(9) == BUZZ);
    assert(fizzBuzz(15) == BUZZ);
    fizzBuzzStr(testStr, 18);
    assertStrEqual(testStr, "Buzz");  
    
    // fizz when divisible by 5 (but not 3))
    assert(fizzBuzz(5) == FIZZ);
    assert(fizzBuzz(25) == FIZZ);
    assert(fizzBuzz(35) == FIZZ);
    fizzBuzzStr(testStr, 40);
    assertStrEqual(testStr, "Fizz"); 
    
    // BuzzFizz when prime (but 3 or 5))
    assert(fizzBuzz(2) == BUZZFIZZ);
    assert(fizzBuzz(7) == BUZZFIZZ);
    assert(fizzBuzz(11) == BUZZFIZZ);
    fizzBuzzStr(testStr, 13);
    assertStrEqual(testStr, "BuzzFizz"); 
    
    // Value otherwise
    assert(fizzBuzz(1) == FIZZ_BUZZ_N_VALUE);
    assert(fizzBuzz(4) == FIZZ_BUZZ_N_VALUE);
    assert(fizzBuzz(22) == FIZZ_BUZZ_N_VALUE);
    fizzBuzzStr(testStr, 14);
    assertStrEqual(testStr, "14"); 
}

void testFibo() {
    
}

void runUnitTests(void) {
    testInputParser();
    testIsPrime();
    testFizzBuzz();
    testFibo();
}